/**
 * 
 */
package org.prelle.splimo.charctrl;

/**
 * @author prelle
 *
 */
public enum SelectionState {

	FIXED,
	SYSTEM_SELECTED,
	USER_SELECTIED
	
}
