package de.rpgframework.splittermond.print.json.model;

import java.util.List;

public class JSONArmor {

    public String name;
    public int defense;
    public int damageReduction;
    public int handicap;
    public int tickMalus;
    public List<JSONFeature> features;
    public boolean relic;
    public boolean personalized;
    public int load;
    public int price;
    public String availability;
    public int quality;
    public String complexity;
    public int durability;
}
