package de.rpgframework.splittermond.print.json.model;

public class JSONFeature {
    public String name;
    public int level;
    public String description;
    public String page;
}
