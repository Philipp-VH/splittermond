package de.rpgframework.splittermond.print.json.model;

import java.util.List;

public class JSONLongRangeWeapon {
    public String name;
    public int value;
    public String skill;
    public String attribute1;
    public String attribute1Id;
    public String attribute2;
    public String attribute2Id;
    public String damage;
    public int weaponSpeed;
    public int characterTickMalus;
    public int calculatedSpeed;
    public int range;
    public List<JSONFeature> features;
    public boolean relic;
    public boolean personalized;
    public int load;
    public int price;
    public String availability;
    public int quality;
    public String complexity;
    public int durability;
}
