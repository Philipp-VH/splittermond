This module contains the JavaFX GUI code for "Splittermond" related character generation. It is intended to be used inside the **Genesis** application of the RPGFramework project.

# Licensing
This code is dual licensed. It can be freely used under the AGPL 3.0 license, but individual licenses for proprietary use are possible.

SPDX-License-Identifier: AGPL-3.0-only OR Unlicensed