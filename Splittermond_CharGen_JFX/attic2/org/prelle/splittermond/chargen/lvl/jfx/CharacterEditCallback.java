/**
 * 
 */
package org.prelle.splittermond.chargen.lvl.jfx;

/**
 * @author prelle
 *
 */
public interface CharacterEditCallback {

	public void dialogClosed(boolean wasCancelled);
	
}
