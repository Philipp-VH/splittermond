/**
 *
 */
package org.prelle.splittermond.chargen.fluent;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface SpliMoCharGenConstants {

	public final static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/splittermond/chargen/jfx/i18n/splimo-chargen");

	public final static String BASE_LOGGER_NAME = "splittermond.jfx";

}
