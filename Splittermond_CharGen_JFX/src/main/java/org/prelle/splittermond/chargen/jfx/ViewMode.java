/**
 * 
 */
package org.prelle.splittermond.chargen.jfx;

/**
 * @author Stefan
 *
 */
public enum ViewMode {

	GENERATION,
	MODIFICATION,
	VIEW_ONLY
	
}
