/**
 *
 */
package org.prelle.splittermond.chargen.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

/**
 * @author prelle
 *
 */
public interface SpliMoCharGenJFXConstants {

	public final static String BASE_LOGGER_NAME = "splittermond.jfx";

	public final static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle(SpliMoCharGenJFXConstants.class.getName());

	public static final String PREFIX = "org/prelle/splittermond/chargen/jfx";

}
