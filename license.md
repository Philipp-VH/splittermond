This repository contains multiple artifacts, that have different licensing models.

| Artifact                 |  License     |
| ------------------------ |:------------:|
| Splittermond_Core        | AGPL-3.0-only OR Unlicensed |
| Splittermond_CharGen     | AGPL-3.0-only OR Unlicensed |
| Splittermond_CharGen_JFX | AGPL-3.0-only OR Unlicensed |
| Splittermond_ProductData | AGPL-3.0-only OR Unlicensed |
| Splittermond_JFX         | AGPL-3.0-only OR Unlicensed |
| Splittermond_Data        | Unlicensed    |

The reason for this is that *Splittermond_Data* contains copyrighted material from the "Uhrwerk Verlag", for which the publisher has granted the RPGFramework usage permissions. Outside this project, the content of *Splittermond_Data* may not be used without consent of the copyright owner and the project owner.